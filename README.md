# DLGCY_WPFPractice

## 介绍

个人 WPF 练习项目及一些小工具，之前是包含在本人的 [Practice](https://gitee.com/dlgcy/Practice) 项目中，现独立出来。

## 相关文章

- [WPF 表格控件 ReoGrid 的简单使用](http://dlgcy.com/reogrid-simple-use/)
- [WPF ComboBox 使用 ResourceBinding 动态绑定资源键并支持语言切换](http://dlgcy.com/wpf-combobox-resource-binding-multi-language)
- [WPF DataGrid 通过自定义表头模拟首行固定](http://dlgcy.com/wpf-datagrid-custom-header-as-fixed-first-row)
- [WPF 触屏事件后触发鼠标事件的问题及 DataGrid 误触问题](http://dlgcy.com/wpf-touch-event-promote-to-touch-event-and-datagrid-touch-problem)
- [WPF DataGrid 如何将被选中行带到视野中](http://dlgcy.com/wpf-datagrid-bring-selected-row-into-view/)
- （整理Demo）[《WPF DataGrid 与﻿ ListView 性能对比与场景选择》](https://www.cnblogs.com/Stay627/p/14270876.html)

## Git子模块

使用参考：《[通过 GitExtensions 来使用 Git 子模块功能](http://dlgcy.com/use-git-submodule-through-gitextensions)》

- [WPFUI](https://gitee.com/dlgcy/WPFUI)
- [WPFTemplateLib](https://gitee.com/dlgcy/WPFTemplateLib)

## 依赖包

- Newtonsoft.Json
- PropertyChanged.Fody
- FreeSpire.PDF
- Microsoft.Xaml.Behaviors.Wpf
- unvell.ReoGridWPF
