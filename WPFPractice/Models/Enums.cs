﻿using System.ComponentModel;

namespace WPFPractice.Models
{
    /// <summary>
    /// 编辑模式枚举
    /// </summary>
    public enum EditTypeEnum
    {
        [Description("展示模式")]
        Show,

        [Description("编辑模式")]
        Edit,

        [Description("新增模式")]
        Add,
    }
}
