﻿using PropertyChanged;

namespace WPFPractice.Models
{
    /// <summary>
    /// 用户类
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public class User
    {
        public EditTypeEnum EditType { get; set; } = EditTypeEnum.Show;

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string Gender { get; set; }

        public int Age { get; set; } = 0;

        public string MobileNum { get; set; }

        public string Address { get; set; }

        public string Signature { get; set; }
    }
}