﻿using DotNet.Utilities;
using PropertyChanged;
using System;
using WPFTemplateLib.WpfHelpers;

namespace WPFPractice.Utils
{
    /// <summary>
    /// 绑定属性基类;
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public abstract class MyBindableBase : BindableBase
    {
        /// <summary>
        /// 信息窗内容;
        /// </summary>
        public string Info { get; set; }

        /// <summary>
        /// 输出信息方法
        /// </summary>
        /// <param name="info">信息内容</param>
        /// <param name="isAddBlankLine">是否空行</param>
        public void ShowInfo(string info, bool isAddBlankLine = false)
        {
            string tail = Environment.NewLine.Repeat(isAddBlankLine ? 2 : 1);
            Info += $"[{DateTime.Now:HH:mm:ss.ffff}] {info}{tail}";
        }

        /// <summary>
        /// 使用信息区用户控件时使用的输出信息方法（此方案暂不可靠，存在信息丢失问题）
        /// </summary>
        /// <param name="info">信息内容</param>
        /// <param name="isAddBlankLine">是否空行</param>
        public void ShowInfoUc(string info, bool isAddBlankLine = false)
        {
            //Info = null;
            string tail = Environment.NewLine.Repeat(isAddBlankLine ? 1 : 0);
            Info = $"[{DateTime.Now:HH:mm:ss.ffff}] {info}{tail}";
        }
    }
}
