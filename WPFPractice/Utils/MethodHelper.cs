﻿using System;
using System.Runtime.CompilerServices;

namespace WPFPractice.Utils
{
    public class MethodHelper
    {
        /// <summary>
        /// 验证参数（不符合条件则抛出异常）
        /// </summary>
        /// <param name="parameterName">参数名</param>
        /// <param name="condition">条件结果（调用时传条件表达式）</param>
        /// <param name="message">消息（无需填写，通过调用方参数表达式功能自动获取条件参数的输入表达式）</param>
        public static void ValidateArgument(string parameterName, bool condition,
            [CallerArgumentExpression("condition")] string? message = null)
        {
            if (!condition)
            {
                throw new ArgumentException($"Argument failed validation: <{message}>", parameterName);
            }
        }
    }
}
