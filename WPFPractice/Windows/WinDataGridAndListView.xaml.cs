﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using WPFPractice.Utils;

namespace WPFPractice.Windows
{
    /// <summary>
    /// [《WPF DataGrid 与﻿ ListView 性能对比与场景选择》](https://www.cnblogs.com/Stay627/p/14270876.html)
    /// </summary>
    public partial class WinDataGridAndListView : Window
    {
        private DgAndLvViewModel _vm;

        public WinDataGridAndListView()
        {
            InitializeComponent();

            _vm = new DgAndLvViewModel();
            _vm.DgList = new ObservableCollection<DgModel>();
            DataContext = _vm;

            InitData();
        }

        
        private void InitData()
        {
            var list = new List<DgModel>();
            Random rd = new Random();

            for (int i = 0; i < 50; i++)
            {
                var item = new DgModel();
                item.F1 = rd.Next().ToString();
                item.F2 = rd.Next().ToString();
                item.F3 = rd.Next().ToString();
                item.F4 = rd.Next().ToString();
                item.F5 = rd.Next().ToString();
                item.F6 = rd.Next().ToString();
                item.F7 = rd.Next().ToString();
                item.F8 = rd.Next().ToString();
                item.F9 = rd.Next().ToString();
                item.F10 = rd.Next().ToString();
                list.Add(item);
            }

            Stopwatch ms = new Stopwatch();
            ms.Restart();
            _vm.DgList = new ObservableCollection<DgModel>(list);
            ms.Stop();
            _vm.DgMs = ms.ElapsedMilliseconds.ToString();

            ms.Restart();
            _vm.LvList = new ObservableCollection<DgModel>(list);
            ms.Stop();
            _vm.LvMs = ms.ElapsedMilliseconds.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            InitData();
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class DgAndLvViewModel : MyBindableBase
    {
        public ObservableCollection<DgModel> DgList { get; set; }

        public ObservableCollection<DgModel> LvList { get; set; }

        public string DgMs { get; set; }

        public string LvMs { get; set; }
    }

    public class DgModel
    {
        public string F1 { get; set; }
        public string F2 { get; set; }
        public string F3 { get; set; }
        public string F4 { get; set; }
        public string F5 { get; set; }
        public string F6 { get; set; }
        public string F7 { get; set; }
        public string F8 { get; set; }
        public string F9 { get; set; }
        public string F10 { get; set; }
    }
}
