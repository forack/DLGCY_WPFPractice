﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PropertyChanged;
using System.Windows;
using System.Windows.Input;
using WPFPractice.Utils;
using WPFTemplateLib.WpfHelpers;
using System;

namespace WPFPractice.Windows
{
    /// <summary>
    /// 各种 TextBox 示例
    /// </summary>
    public partial class WinTextBox : Window
    {
        private TextBoxViewModel _VM;

        public WinTextBox()
        {
            InitializeComponent();
            _VM = new TextBoxViewModel();
            DataContext = _VM;
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class TextBoxViewModel : MyBindableBase
    {
        #region 成员

        /// <summary>
        /// 状态信息
        /// </summary>
        public string StatusInfo { get; set; }

        #endregion

        public TextBoxViewModel()
        {
            SetCommandMethod();
        }

        #region 辅助方法

        public void ShowInfo(string info)
        {
            StatusInfo = $"{info}";
        }

        #endregion

        #region 绑定

        private string _NumLeft = "0";
        public string NumLeft
        {
            get => _NumLeft;
            set
            {
                ValidateBlank(value, "左运算数不能为空");
                SetProperty(ref _NumLeft, value);
            }
        }

        private string _NumRight = "0";
        public string NumRight
        {
            get => _NumRight;
            set
            {
                ValidateBlank(value, "右运算数不能为空");
                SetProperty(ref _NumRight, value);
            }
        }

        public int NumAddResult { get; set; }

        #endregion

        #region 命令

        public ICommand AddCmd { get; set; }

        #endregion

        /// <summary>
        /// 命令方法赋值（在构造方法中调用）
        /// </summary>
        private void SetCommandMethod()
        {
            AddCmd ??= new RelayCommand(o => true, async o =>
            {
                List<string> propNameList = new List<string>() { nameof(NumLeft), nameof(NumRight) };
                if (IsContainErrors(propNameList))
                {
                    var errors = GetErrors(propNameList);
                    MessageBox.Show($"{string.Join(Environment.NewLine, errors)}");
                    return;
                }

                NumAddResult = 0;
                await Task.Delay(200);
                NumAddResult = int.Parse(NumLeft) + int.Parse(NumRight);
            });
        }
    }
}
