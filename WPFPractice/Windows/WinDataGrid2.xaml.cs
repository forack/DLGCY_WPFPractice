﻿using DotNet.Utilities.ConsoleHelper;
using PropertyChanged;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using DotNet.Utilities;
using WPFPractice.Models;
using WPFPractice.Utils;
using WPFTemplateLib.WpfHelpers;
using System.Windows.Controls;

namespace WPFPractice.Windows
{
    public partial class WinDataGrid2 : Window
    {
        private DataGrid2ViewModel _vm;

        public WinDataGrid2()
        {
            InitializeComponent();
            DataContext = _vm = new DataGrid2ViewModel();

            _vm.SelectedItemChanged += OnSelectedItemChanged;
        }

        /// <summary>
        /// 选中项改变事件执行方法
        /// </summary>
        /// <param name="index">选中行索引</param>
        private void OnSelectedItemChanged(int index)
        {
            Dg.ScrollIntoView(Dg.Items.GetItemAt(index));
        }

        private void EventSetter_LostFocus(object sender, RoutedEventArgs e)
        {
            var selected = _vm.SelectedItem;
            if(selected != null)
            {
                //selected.EditType = EditTypeEnum.Show;
            }
        }

        private void Dg_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (sender)
            {
                //只在点击表格其它地方时执行某些操作，点击行时不处理;
                case DataGrid dg:
                    //想要进行的操作写这里;

                    MessageBox.Show("点击了表格空白处");

                    //如果是自动列，使用此方法可以使选中的单元格进入编辑模式，但是不知道如何使整行进入编辑模式;
                    dg.BeginEdit();

                    break;
                case DataGridRow row:
                    e.Handled = true; //标记为已处理，就不会触发之后的 DataGrid 的 MouseDown 事件;
                    break;
                default:
                    break;
            }
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class DataGrid2ViewModel : MyBindableBase
    {
        #region 成员

        /// <summary>
        /// 改变前的对象引用
        /// </summary>
        private User _originUser;

        #endregion

        #region Bindable

        /// <summary>
        /// 表格数据
        /// </summary>
        public ObservableCollection<User> Datas { get; set; }

        /// <summary>
        /// 选中项改变事件
        /// </summary>
        public event Action<int> SelectedItemChanged;

        private User _SelectedItem;
        /// <summary>
        /// 选中项
        /// </summary>
        public User SelectedItem
        {
            get => _SelectedItem;
            set
            {
                SetProperty(ref _SelectedItem, value);

                SelectedItemChanged?.Invoke(Datas.IndexOf(_SelectedItem));
            }
        }

        private EditTypeEnum _editType = EditTypeEnum.Show;
        /// <summary>
        /// 编辑模式
        /// </summary>
        public EditTypeEnum EditType
        {
            get => _editType;
            set
            {
                _editType = value;
                ShowInfo($"切换到 {_editType.GetEnumDescription()}");
            }
        }

        /// <summary>
        /// 是否显示个性签名
        /// </summary>
        public bool IsShowSignature { get; set; } = true;

        /// <summary>
        /// 指定的选中索引
        /// </summary>
        public int SelectedNo { get; set; } = 1;

        #endregion

        #region Command

        /// <summary>
        /// 切换到新增模式命令
        /// </summary>
        public ICommand AddNewCmd { get; set; }

        /// <summary>
        /// 添加用户命令
        /// </summary>
        public ICommand AddUserCmd { get; set; }

        /// <summary>
        /// 表格选择行变化事件触发命令
        /// </summary>
        public ICommand SelectionChangedCmd { get; set; }

        /// <summary>
        /// 是否能执行选择行变化命令
        /// </summary>
        public bool IsCanSelectionChanged { get; set; } = true;

        /// <summary>
        /// 删除命令
        /// </summary>
        public ICommand DeleteCmd { get; set; }

        /// <summary>
        /// 显示隐藏个性签名命令
        /// </summary>
        public ICommand ShowOrHideSignCmd { get; set; }

        /// <summary>
        /// 进入编辑模式命令
        /// </summary>
        public ICommand EnterEditCmd { get; set; }

        /// <summary>
        /// 失去焦点事件命令
        /// </summary>
        public ICommand LostFocusCmd { get; set; }

        /// <summary>
        /// 选中指定序号条目的命令
        /// </summary>
        public ICommand SelectCmd { get; set; }

        #endregion

        public DataGrid2ViewModel()
        {
            SetCommandMethod();

            InitValue();

            Console.SetOut(new ConsoleWriter(s =>
            {
                ShowInfo(s);
            }));
        }

        /// <summary>
        /// 数据初始化
        /// </summary>
        private void InitValue()
        {
            ShowInfo("演示 DataGrid 行内编辑、滚动到选中项");
            Datas = new ObservableCollection<User>()
            {
                new User(){UserId = "01", UserName = "张三", Gender = "男", Age = 20, MobileNum = "13597362875", Address = "浙江省杭州市西湖区古墩路", Signature = "好好学习，天天向上"},
                new User(){UserId = "02", UserName = "李四", Gender = "男", Age = 22, MobileNum = "13385937628", Address = "福建省南平市邵武市水北镇", Signature = "生命不息，奋斗不止"},
                new User(){UserId = "03", UserName = "王妩", Gender = "女", Age = 24, MobileNum = "17129463982", Address = "美利坚共和国加利福尼亚洲", Signature = "天妒英才，命运多舛"},
                new User(){UserId = "04", UserName = "赵溜", Gender = "男", Age = 28, MobileNum = "18868839476", Address = "火星基地一号华中区杭州堡", Signature = "人生一世，草木一秋"},
                new User(){UserId = "05", UserName = "钱七", Gender = "男", Age = 38, MobileNum = "18868839476", Address = "", Signature = ""},
                new User(){UserId = "06", UserName = "孙子", Gender = "男", Age = 08, MobileNum = "18868839476", Address = "", Signature = ""},
                new User(){UserId = "07", UserName = "李老", Gender = "男", Age = 48, MobileNum = "18868839476", Address = "", Signature = ""},
                new User(){UserId = "08", UserName = "魏王", Gender = "男", Age = 58, MobileNum = "18868839476", Address = "", Signature = ""},
                new User(){UserId = "09", UserName = "九九", Gender = "男", Age = 99, MobileNum = "18868839476", Address = "", Signature = ""},
                new User(){UserId = "10", UserName = "石海", Gender = "男", Age = 98, MobileNum = "18868839476", Address = "", Signature = ""},
            };
        }

        /// <summary>
        /// 命令方法赋值(在构造函数中调用)
        /// </summary>
        private void SetCommandMethod()
        {
            AddNewCmd ??= new RelayCommand(o => true, o =>
            {
                EditType = EditTypeEnum.Add;
                SelectedItem = new User();
            });

            AddUserCmd ??= new RelayCommand(o => !(string.IsNullOrEmpty(SelectedItem?.UserId) || string.IsNullOrEmpty(SelectedItem?.UserName)), o =>
            {
                Datas.Add(SelectedItem);
                ShowInfo($"已添加用户 {SelectedItem.UserId}");

                EditType = EditTypeEnum.Show;
            });

            SelectionChangedCmd ??= new RelayCommand(o => IsCanSelectionChanged, o =>
            {
                try
                {
                    IsCanSelectionChanged = false;

                    var args = o as SelectionChangedEventArgs;
                    EditType = EditTypeEnum.Show;

                    if (_originUser != null)
                    {
                        _originUser.EditType = EditTypeEnum.Show;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                finally
                {
                    _originUser = SelectedItem;

                    IsCanSelectionChanged = true;
                }
            });

            DeleteCmd ??= new RelayCommand(o => true, o =>
            {
                var user = o as User;
                if(user == null)
                {
                    return;
                }

                Datas.Remove(user);
                ShowInfo($"已删除用户[{user.UserId}]");
                EditType = EditTypeEnum.Show;
            });

            ShowOrHideSignCmd ??= new RelayCommand(o => true, o =>
            {
                IsShowSignature = !IsShowSignature;
            });

            EnterEditCmd ??= new RelayCommand(o => true, o =>
            {
                EditType = EditTypeEnum.Edit;
                if (SelectedItem != null)
                {
                    SelectedItem.EditType = EditType;
                }
            });

            LostFocusCmd ??= new RelayCommand(o => true, o =>
            {
                var args = o as RoutedEventArgs;

                if (args.Source.GetType() == typeof(DataGrid) && SelectedItem != null)
                {
                    //SelectedItem.EditType = EditTypeEnum.Show;
                }
            });

            SelectCmd ??= new RelayCommand(o => true, o =>
            {
                try
                {
                    var selected = Datas[SelectedNo-1];
                    SelectedItem = selected;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"选择命令执行异常：{ex}");
                }
            });
        }
    }
}
