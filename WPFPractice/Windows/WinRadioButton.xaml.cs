﻿using DotNet.Utilities.ConsoleHelper;
using PropertyChanged;
using System;
using System.Windows;
using System.Windows.Input;
using WPFPractice.Utils;
using WPFTemplateLib.WpfHelpers;

namespace WPFPractice.Windows
{
    public partial class WinRadioButton : Window
    {
        private RadioButtonViewModel _vm;

        public WinRadioButton()
        {
            InitializeComponent();
            DataContext = _vm = new RadioButtonViewModel();
        }

        private bool _lastChecked;
        private void RbCanUncheck_OnClick(object sender, RoutedEventArgs e)
        {
            if (_lastChecked)
            {
                RbCanUncheck.IsChecked = false;
                _lastChecked = false;
            }
            else
            {
                RbCanUncheck.IsChecked = true;
                _lastChecked = true;
            }
        }
    }

    [AddINotifyPropertyChangedInterface]
    public class RadioButtonViewModel : MyBindableBase
    {
        #region 成员

        #endregion

        #region Bindable

        #endregion

        #region Command

        /// <summary>
        /// 切换到新增模式命令
        /// </summary>
        public ICommand AddNewCmd { get; set; }

        #endregion

        public RadioButtonViewModel()
        {
            SetCommandMethod();

            InitValue();

            Console.SetOut(new ConsoleWriter(s =>
            {
                ShowInfo(s);
            }));
        }

        /// <summary>
        /// 数据初始化
        /// </summary>
        private void InitValue()
        {
            ShowInfo("演示 RadioButton 取消选中");
        }

        /// <summary>
        /// 命令方法赋值(在构造函数中调用)
        /// </summary>
        private void SetCommandMethod()
        {
            AddNewCmd ??= new RelayCommand(o => true, o =>
            {
                
            });
        }
    }
}
