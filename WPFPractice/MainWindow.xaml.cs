﻿using System.Windows;
using WPFPractice.Windows;

namespace WPFPractice
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnTextBox_OnClick(object sender, RoutedEventArgs e)
        {
            new WinTextBox().Show();
        }

        private void BtnSpirePdf_Click(object sender, RoutedEventArgs e)
        {
            new WinSpirePdf().Show();
        }

        private void BtnDingTalk_OnClick(object sender, RoutedEventArgs e)
        {
            new WinDingTalk().Show();
        }

        private void BtnMoveResize_OnClick(object sender, RoutedEventArgs e)
        {
            new WinButtonMoveResize().Show();
        }

        private void BtnReoGrid_OnClick(object sender, RoutedEventArgs e)
        {
            new WinReoGrid().Show();
        }

        private void BtnPathGeometry_OnClick(object sender, RoutedEventArgs e)
        {
            new WinPathGeometry().Show();
        }

        private void BtnGetLastInputInfo_OnClick(object sender, RoutedEventArgs e)
        {
            new WinGetLastInputInfo().Show();
        }

        private void BtnComboBox_OnClick(object sender, RoutedEventArgs e)
        {
            new WinComboBox().Show();
        }

        private void BtnDataGrid_OnClick(object sender, RoutedEventArgs e)
        {
            new WinDataGrid().Show();
        }

        private void BtnDataGrid2_OnClick(object sender, RoutedEventArgs e)
        {
            new WinDataGrid2().Show();
        }

        private void BtnDgAndLv_OnClick(object sender, RoutedEventArgs e)
        {
            new WinDataGridAndListView().Show();
        }

        private void BtnRadioButton_OnClick(object sender, RoutedEventArgs e)
        {
            new WinRadioButton().Show();
        }

        private void BtnCSharpTester_OnClick(object sender, RoutedEventArgs e)
        {
            new WinCSharpTester().Show();
        }

        private void BtnTaskTester_OnClick(object sender, RoutedEventArgs e)
        {
            new WinTaskTester().Show();
        }

        private void BtnMsiInstaller_OnClick(object sender, RoutedEventArgs e)
        {
            new WinMsiInstaller().Show();
        }
    }
}
